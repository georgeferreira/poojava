package ExercícioPessoas;

public class Main {
    public static void main(String[] args) {
        Pessoa p1 = new Pessoa();
        Aluno p2 = new Aluno();
        Professor p3 = new Professor();
        Funcionario p4 = new Funcionario();

        p1.setNomePessoa("George");
        p1.setIdadePessoa(27);
        p1.setSexoPessoa("Masculino");

        p2.setNomePessoa("Maria");
        p2.setIdadePessoa(20);
        p2.setSexoPessoa("Feminino");
        p2.setCurso("Sistemas de Informação");
        p2.setMatricula(163259);

        p3.setNomePessoa("Pedriana");
        p3.setSalario(10000.00f);
        p3.setEspecialidade("Ciencista da Computação");
        p3.setIdadePessoa(30);
        p3.setSexoPessoa("Feminino");

        p4.setNomePessoa("Xablau");
        p4.setTrabalhandoAtualmente(true);
        p4.setSetor("Administratibo");
        p4.setIdadePessoa(45);
        p4.setSexoPessoa("Feminino");

        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());

    }
}
