package ExercícioPessoas;

public class Funcionario extends Pessoa {
    private String setor;
    private boolean trabalhandoAtualmente;

    public void mudarTrabalho(){
        this.trabalhandoAtualmente = !this.trabalhandoAtualmente;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public boolean getTrabalhandoAtualmente() {
        return trabalhandoAtualmente;
    }

    public void setTrabalhandoAtualmente(boolean trabalhandoAtualmente) {
        this.trabalhandoAtualmente = trabalhandoAtualmente;
    }
}
