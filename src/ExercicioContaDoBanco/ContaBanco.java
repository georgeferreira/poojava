package ExercicioContaDoBanco;

public class ContaBanco {
	public int numeroConta;
	protected String tipoConta;
	private String titularConta;
	private float saldoConta;
	private boolean status;
	
	public void estadoAtual() {
		System.out.println("######################################");
		System.out.println("Conta: " + this.getNumeroConta());
		System.out.println("Tipo: " + this.getTipoConta());
		System.out.println("Titular: " + this.getTitularConta());
		System.out.println("Saldo: R$" + this.getSaldoConta());
		System.out.println("Status: " + this.getStatus());
	}
	
	public void abrirConta(String tipo) {
		this.setTipoConta(tipo);
		this.setStatus(true);
		if (tipo == "contaCorrente") {
			this.setSaldoConta(50.00f);
		} else if(tipo == "contaPoupanca") {
			this.setSaldoConta(150.00f);
		}
	}

	public void fecharConta() {
		if(this.getSaldoConta() > 0) {
			System.out.println("Conta não pode ser fechada, pois exite saldo");
		} else if (this.getSaldoConta() < 0) {
			System.out.println("Conta não pode ser fechada, pois exite saldo negativo");
		} else {
			System.out.println("Conta Fechada com sucesso!");
		}
	}
	
	public void depositarValor(float valor) {
		if (this.getStatus()) {
			this.setSaldoConta(this.getSaldoConta() + valor);
			System.out.println(this.getTitularConta() + " , seu depósito realizado com sucesso!");
		} else {
			System.out.println("Impossível realizar esta operação. Conta fechada ou inexistente.");
		}
	}
	
	public void sacarValor(float valor) {
		if(this.getStatus()) {
			if(this.getSaldoConta() >= valor) {
				this.setSaldoConta(this.getSaldoConta() - valor);
				System.out.println("Saque realizado com sucesso. Meu novo saldo é de: R$ " + this.getSaldoConta());
			} else {
				System.out.println("Saldo insuficiente para esta operação.");
			}
		}else {
			System.out.println("Impossível realizar esta operação. Conta fechada ou inexistente.");
		}
	}
	
	public void pagarTaxaDeManutencao() {
		int valorTaxaDeManutencao = 0;
		if(this.getTipoConta() ==  "contaCorrente") {
			valorTaxaDeManutencao = 12;
		} else if(this.getTipoConta() ==  "contaPoupanca"){
			valorTaxaDeManutencao = 20;
		}
		if(this.getStatus()) {
			this.setSaldoConta(this.getSaldoConta() - valorTaxaDeManutencao);
			System.out.println("Olá, " + this.getTitularConta() + ". A taxa de manutenção mensal de R$" + valorTaxaDeManutencao
					+ ". Seu saldo atual é de R$" + this.getSaldoConta());
		}
	}
	
	public ContaBanco() { // Método Construtor
		this.setSaldoConta(0.00f);
		this.setStatus(false);
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	public String getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getTitularConta() {
		return titularConta;
	}

	public void setTitularConta(String titularConta) {
		this.titularConta = titularConta;
	}

	public float getSaldoConta() {
		return saldoConta;
	}

	public void setSaldoConta(float saldoConta) {
		this.saldoConta = saldoConta;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
