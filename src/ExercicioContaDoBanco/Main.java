package ExercicioContaDoBanco;

public class Main {
	public static void main(String[] args) {
		ContaBanco Titular1 = new ContaBanco();
		Titular1.setNumeroConta(2901);
		Titular1.setTitularConta("George Ferreira");
		Titular1.abrirConta("contaCorrente");		
		
		ContaBanco Titular2 = new ContaBanco();
		Titular2.setNumeroConta(1993);
		Titular2.setTitularConta("Xablau da Silva");
		Titular2.abrirConta("contaPoupanca");
		
		
		Titular1.setSaldoConta(595.7f);
		Titular2.setSaldoConta(51651.9f);
		
		Titular2.sacarValor(100.3f);;
		
		Titular1.pagarTaxaDeManutencao();
		
		Titular1.fecharConta();
		
		Titular1.estadoAtual();
		Titular2.estadoAtual();
	}
}
