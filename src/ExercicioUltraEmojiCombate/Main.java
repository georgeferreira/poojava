package ExercicioUltraEmojiCombate;

public class Main {
    public static void main(String[] args) {
        Lutador[] l = new Lutador [6];
        l[0] = new Lutador("George Ferreira", "Brasileiro", 27,
                1.70f, 100.00f, 10, 2,
                1);
        l[1] = new Lutador(
                "Ferreira George", "Argentino", 27,
                1.70f, 120.00f, 10, 2,
                1);
        l[2] = new Lutador(
                "Xablau", "Francês", 27,
                1.70f, 69.00f, 10, 2,
                1);
        l[3] = new Lutador(
                "ItHappens", "Americano", 27,
                1.70f, 890.00f, 10, 2,
                1);
        l[4] = new Lutador(
                "Pulse", "Inglês", 27,
                1.70f, 70.00f, 10, 2,
                1);
        l[5] = new Lutador(
                "Armazen Mateus", "Irlandês", 27,
                1.70f, 91.00f, 10, 2,
                1);

        Luta UEC01 =  new Luta();
        UEC01.marcarLuta(l[0],l[1] );
        UEC01.lutar();
    }
}
