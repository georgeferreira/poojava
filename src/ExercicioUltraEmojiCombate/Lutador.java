package ExercicioUltraEmojiCombate;

public class Lutador {

    private String nome, nacionalidade;
    private int idadeLutador;
    private float alturaLutador;
    private float pesoLutador;
    private String categoriaLutador;
    private int qtdVitoriaLutador;
    private int qtdDerrotaLutador;
    private int qtdEmpateLutador;

    public void apresentar() {
        System.out.println("Nome: " + this.getNome());
        System.out.println("Idade: " + this.getIdadeLutador());
        System.out.println("Nacionalidade: " + this.getNacionalidade());
        System.out.println("Altura: " + this.getAlturaLutador());
        System.out.println("Peso: " + this.getPesoLutador());
        System.out.println("Vitórias: " + this.getQtdVitoriaLutador());
        System.out.println("Empates: " + this.getQtdEmpateLutador());
        System.out.println("Derrotas: " + this.getQtdDerrotaLutador());
    }

    public void status() {

    }

    public void ganharLuta() {
        this.setQtdVitoriaLutador(this.getQtdVitoriaLutador() + 1);
    }

    public void perderLuta() {
        this.setQtdDerrotaLutador(this.getQtdDerrotaLutador() + 1);
    }

    public void empatarLuta() {
        this.setQtdEmpateLutador(this.getQtdEmpateLutador() + 1);
    }

    public Lutador(String nome,
                   String nacionalidade,
                   int idadeLutador,
                   float alturaLutador,
                   float pesoLutador,
                   int qtdVitoriaLutador,
                   int qtdDerrotaLutador,
                   int qtdEmpateLutador) {
        this.nome = nome;
        this.nacionalidade = nacionalidade;
        this.idadeLutador = idadeLutador;
        this.alturaLutador = alturaLutador;
        this.setPesoLutador(pesoLutador);
        this.qtdVitoriaLutador = qtdVitoriaLutador;
        this.qtdDerrotaLutador = qtdDerrotaLutador;
        this.qtdEmpateLutador = qtdEmpateLutador;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public int getIdadeLutador() {
        return idadeLutador;
    }

    public void setIdadeLutador(int idadeLutador) {
        this.idadeLutador = idadeLutador;
    }

    public float getAlturaLutador() {
        return alturaLutador;
    }

    public void setAlturaLutador(float alturaLutador) {
        this.alturaLutador = alturaLutador;
    }

    public float getPesoLutador() {
        return pesoLutador;
    }

    public void setPesoLutador(float pesoLutador) {
        this.pesoLutador = pesoLutador;
        this.setCategoriaLutador();
    }

    public String getCategoriaLutador() {
        return categoriaLutador;
    }

    private void setCategoriaLutador() {

        if (this.pesoLutador < 52.5) {
            this.categoriaLutador = "Inválido";
        } else if (this.pesoLutador <= 70.3) {
            this.categoriaLutador = "Peso Leve";
        } else if (this.pesoLutador < 83.9) {
            this.categoriaLutador = "Peso Médio";
        } else if (this.pesoLutador <= 120.2) {
            this.categoriaLutador = "Peso Pesado";
        } else {
            this.categoriaLutador = "Inválido";
        }
    }

    public int getQtdVitoriaLutador() {
        return qtdVitoriaLutador;
    }

    public void setQtdVitoriaLutador(int qtdVitoriaLutador) {
        this.qtdVitoriaLutador = qtdVitoriaLutador;
    }

    public int getQtdDerrotaLutador() {
        return qtdDerrotaLutador;
    }

    public void setQtdDerrotaLutador(int qtdDerrotaLutador) {
        this.qtdDerrotaLutador = qtdDerrotaLutador;
    }

    public int getQtdEmpateLutador() {
        return qtdEmpateLutador;
    }

    public void setQtdEmpateLutador(int qtdEmpateLutador) {
        this.qtdEmpateLutador = qtdEmpateLutador;
    }
}
