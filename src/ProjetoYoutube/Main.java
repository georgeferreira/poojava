package ProjetoYoutube;

public class Main {
    public static void main(String[] args) {
        Video video[]= new Video[2];
        video[0] = new Video("Aula de Orientação a Objetos");
        video[1] = new Video("Aula de Java");

        Aluno aluno[] = new Aluno[2];
        aluno[0] = new Aluno("George", 27, "M",
                "georgeferreira");
        aluno[1] = new Aluno("Xablau", 27, "M",
                "georgeferreira");

//        System.out.println(video[0].toString());
//        System.out.println("\n##########################\n");
//        System.out.println(aluno[0].toString());

        Visualizacao visualizacao[] = new Visualizacao[2];
        visualizacao[0] = new Visualizacao(aluno[0], video[0]);
        System.out.println(visualizacao[0].toString());
        System.out.println("##################");
        visualizacao[1] = new Visualizacao(aluno[1], video[0]);
        System.out.println(visualizacao[1].toString());
    }
}
