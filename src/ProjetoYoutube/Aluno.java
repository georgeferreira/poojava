package ProjetoYoutube;

public class Aluno extends Pessoa{
    private String login;
    private int totalAssistido;

    public Aluno(String nome, int idade, String sexo, String login) {
        super(nome, idade, sexo);
        this.login = login;
        this.totalAssistido = 0;
    }

    public void maisUmaView(){
        System.out.println("teste");
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getAssistindoAula() {
        return totalAssistido;
    }

    public void setAssistindoAula(int assistindoAula) {
        this.totalAssistido = assistindoAula;
    }

    @Override
    public String toString() {
        return "Aluno{\n" + super.toString() +
                "login='" + login + '\'' +
                ", totalAssistido=" + totalAssistido +
                '}';
    }
}
