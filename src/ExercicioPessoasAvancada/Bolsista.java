package ExercicioPessoasAvancada;

public class Bolsista extends Aluno {
    private float bolsa;

    public void renovarBolsa() {

    }

    @Override
    public void pagarMensalidade() {
        super.pagarMensalidade();
        System.out.println(this.getNome() + " é bolsista! Isento de pagamento.");
    }

    public float getBolsa() {
        return bolsa;
    }

    public void setBolsa(float bolsa) {
        this.bolsa = bolsa;
    }
}
