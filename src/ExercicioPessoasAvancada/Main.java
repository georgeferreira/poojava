package ExercicioPessoasAvancada;

public class Main {
    public static void main(String[] args) {
        Visitante v1 = new Visitante();
        v1.setNome("George");
        v1.setIdade(27);
        v1.setSexo("Masculino");
        System.out.println(v1.toString());

        System.out.println("----------------");

        Aluno a1 = new Aluno();
        a1.setNome("Xablau");
        a1.setIdade(30);
        a1.setSexo("Masculino");
        a1.setCurso("Sistemas de Informação");
        a1.setMatricula(16325);
        a1.pagarMensalidade();
        System.out.println(a1.toString());

        System.out.println("----------------");

        Bolsista b1 = new Bolsista();
        b1.setNome("Ferreira");
        b1.setIdade(20);
        b1.setSexo("Feminino");
        b1.setCurso("Ciência da Computação");
        b1.setMatricula(85467);
        b1.setBolsa(2500.5f);
        b1.pagarMensalidade();

        System.out.println("----------------");


    }
}
