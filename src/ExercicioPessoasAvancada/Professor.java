package ExercicioPessoasAvancada;

public class Professor extends Pessoa{
    private String especialidade;
    private float salarip;

    public void receberAumento(){}

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public float getSalarip() {
        return salarip;
    }

    public void setSalarip(float salarip) {
        this.salarip = salarip;
    }
}
