package ExercicioAnimalPolimorfismoSobreposicao;

import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Ave;
import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Mamifero;
import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Peixe;
import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Reptil;

public class Main {
    public static void main(String[] args) {
        Mamifero m = new Mamifero();
        Reptil r = new Reptil();
        Peixe p = new Peixe();
        Ave a = new Ave();

        Arara arara = new Arara();
        Cachorro cachorro = new Cachorro();
        Canguru canguru = new Canguru();
        Cobra cobra = new Cobra();
        GoldFish goldFish = new GoldFish();
        Tartaruga tartaruga = new Tartaruga();

        cachorro.emitirSom();
        cachorro.locomover();
    }
}
