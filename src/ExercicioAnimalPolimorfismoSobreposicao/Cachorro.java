package ExercicioAnimalPolimorfismoSobreposicao;

import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Mamifero;

public class Cachorro extends Mamifero {
    public void enterrarOsso(){
        System.out.println("Enterrando o osso");
    }

    @Override
    public void emitirSom() {
        super.emitirSom();
        System.out.println("Au au");
    }
}
