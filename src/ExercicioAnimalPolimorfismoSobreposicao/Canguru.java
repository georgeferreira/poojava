package ExercicioAnimalPolimorfismoSobreposicao;

import ExercicioAnimalPolimorfismoSobreposicao.AnimalModel.Mamifero;

public class Canguru extends Mamifero {
    public void usarBolsa(){
        System.out.println("Usando a bolsa natural");
    }

    @Override
    public void locomover() {
        System.out.println("Saltando");
    }
}
