package ExercicioAnimalPolimorfismoSobreposicao.AnimalModel;

public class Mamifero extends Animal{

    private String corDoPelo;

    public String getCorDoPelo() {
        return corDoPelo;
    }

    public void setCorDoPelo(String corDoPelo) {
        this.corDoPelo = corDoPelo;
    }

    public void status(){
        System.out.println("A cor do pelo é: " + this.getCorDoPelo());
        System.out.println("O peso do animal é: " + this.getPesoAnimal());
        System.out.println("A idade do animal é: " + this.getIdadeAnimal());
        System.out.println("A quantidade de membros do animal é: " + this.getMembrosDoAnimal());

    }

    @Override
    public void locomover() {
        System.out.println("Correndo");
    }

    @Override
    public void alimentar() {
        System.out.println("Mamando");
    }

    @Override
    public void emitirSom() {
        System.out.println("Som do Mamífero");
    }
}
