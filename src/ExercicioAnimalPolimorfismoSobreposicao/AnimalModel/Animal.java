package ExercicioAnimalPolimorfismoSobreposicao.AnimalModel;

public abstract class Animal {
    protected float pesoAnimal;
    protected int idadeAnimal;
    protected int membrosDoAnimal;

    public abstract void locomover();
    public abstract void alimentar();
    public abstract void emitirSom();

    public float getPesoAnimal() {
        return pesoAnimal;
    }

    public void setPesoAnimal(float pesoAnimal) {
        this.pesoAnimal = pesoAnimal;
    }

    public int getIdadeAnimal() {
        return idadeAnimal;
    }

    public void setIdadeAnimal(int idadeAnimal) {
        this.idadeAnimal = idadeAnimal;
    }

    public int getMembrosDoAnimal() {
        return membrosDoAnimal;
    }

    public void setMembrosDoAnimal(int membrosDoAnimal) {
        this.membrosDoAnimal = membrosDoAnimal;
    }
}
