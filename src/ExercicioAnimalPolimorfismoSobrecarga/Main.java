package ExercicioAnimalPolimorfismoSobrecarga;

public class Main {
    public static void main(String[] args) {
        Lobo lobo = new Lobo();
        Cachorro cachorro = new Cachorro();

        lobo.emitirSom();
        cachorro.emitirSom();
        cachorro.reagir("Quer ração?");
        cachorro.reagir(false);

    }
}
