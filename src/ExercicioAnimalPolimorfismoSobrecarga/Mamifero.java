package ExercicioAnimalPolimorfismoSobrecarga;

public class Mamifero extends Animal{
    protected String corDoPelo;

    @Override
    public void emitirSom() {
        System.out.println("Som do Mamífero");
    }
}
