package ExercicioAnimalPolimorfismoSobrecarga;

public abstract class Animal {
    protected float pesoAnimal;
    protected int idadeAnimal;
    protected int membrosDoAnimal;

    public abstract void emitirSom();
}
