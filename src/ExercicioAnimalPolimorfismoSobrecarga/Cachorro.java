package ExercicioAnimalPolimorfismoSobrecarga;

public class Cachorro extends Lobo{
    @Override
    public void emitirSom() {
//        super.emitirSom();
        System.out.println("Au au au");
    }

    public void reagir(String frase){
        if (frase.equals("Hora do rango") || frase.equals("Quer ração?")) {
            System.out.println("Abanar e Latir");
        } else {
            System.out.println("Rosnar");
        }
    }
    public void reagir(boolean dono){
        if (dono){
            System.out.println("Abanar");
        } else {
            System.out.println("Rosnar e Latir");
            this.emitirSom();
        }
    }
}
