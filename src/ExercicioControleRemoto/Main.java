package ExercicioControleRemoto;

public class Main {
    public static void main(String[] args) {
        ControleRemoto Controle = new ControleRemoto();

        Controle.ligar();
        Controle.aumentarVolume();
        Controle.abrirMenu();
        Controle.fecharMenu();
    }

}
