package ExercicioEditora;

public class Main {
    public static void main(String[] args) {
        Pessoa[] p  = new Pessoa[2];
        Livro[] l = new Livro[3];

        p[0] = new Pessoa("George", 27, "Masculino");
        p[1] = new Pessoa("Xablau", 20, "Masculino");

        l[0] = new Livro("Java POO Básico", "Ferreira George", 365, p[0]);
        l[1] = new Livro("Java POO Intermediário", "Patricia de Olivia", 630, p[0]);
        l[2] = new Livro("Java POO Avançado", "Bill Gates", 430, p[1]);

        System.out.println(l[2].detalhesDoLivro());
    }
}
