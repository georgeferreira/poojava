package ExercicioEditora;

public class Pessoa {
    private String nomePessoa;
    private int idadePessoa;
    private String sexoPessoa;

    public void fazerAniversario(){

    }

    public Pessoa(String nomePessoa, int idadePessoa, String sexoPessoa) {
        this.nomePessoa = nomePessoa;
        this.idadePessoa = idadePessoa;
        this.sexoPessoa = sexoPessoa;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public int getIdadePessoa() {
        return idadePessoa;
    }

    public void setIdadePessoa(int idadePessoa) {
        this.idadePessoa = idadePessoa;
    }

    public String getSexoPessoa() {
        return sexoPessoa;
    }

    public void setSexoPessoa(String sexoPessoa) {
        this.sexoPessoa = sexoPessoa;
    }

}
