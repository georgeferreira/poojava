package ExercicioEditora;

public class Livro implements Publicacao{
    private String tituloLivro;
    private String autorLivro;
    private int totalPaginas;
    private int paginaAtual;
    private boolean livroAberto;
    private Pessoa leitor;

    public String detalhesDoLivro(){
        return "Livro{" + "Titulo=" + tituloLivro + "\n, autor=" + autorLivro + "\n, Total de Paginas=" + totalPaginas + "\n, na Página Atual="
                + paginaAtual + "\n, aberto=" + livroAberto + "\n, e o leitor é=" + leitor.getNomePessoa();
    }

    public Livro(String tituloLivro, String autorLivro, int totalPaginas, Pessoa leitor) {
        this.tituloLivro = tituloLivro;
        this.autorLivro = autorLivro;
        this.totalPaginas = totalPaginas;
        this.paginaAtual = 0;
        this.livroAberto = false;
        this.leitor = leitor;
    }

    public String getTituloLivro() {
        return tituloLivro;
    }

    public void setTituloLivro(String tituloLivro) {
        this.tituloLivro = tituloLivro;
    }

    public String getAutorLivro() {
        return autorLivro;
    }

    public void setAutorLivro(String autorLivro) {
        this.autorLivro = autorLivro;
    }

    public int getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public Pessoa getLeitor() {
        return leitor;
    }

    public void setLeitor(Pessoa leitor) {
        this.leitor = leitor;
    }

    @Override
    public void abrir() {
        this.livroAberto = true;
    }

    @Override
    public void fechar() {
        this.livroAberto = false;
    }

    @Override
    public void folhear(int pagina) {
        this.paginaAtual = pagina;
    }

    @Override
    public void avancarPagina() {
        this.paginaAtual++;
    }

    @Override
    public void voltarPagina() {
        this.paginaAtual--;
    }
}
